db.fruits.insertMany([
{
	"name":"Banana",
	"supplier":"Farmer Fruits Inc.",
	"stocks":30,
	"price":20,
	"onSale":true
},
{
	"name":"Mango",
	"supplier":"Mango Magic Inc.",
	"stocks":50,
	"price":70,
	"onSale":true
},
{
	"name":"Dragon Fruit",
	"supplier":"Farmer Fruits Inc.",
	"stocks":10,
	"price":60,
	"onSale":true
},
{
	"name":"Grapes",
	"supplier":"Fruity Co.",
	"stocks":30,
	"price":100,
	"onSale":true
},
{
	"name":"Apple",
	"supplier":"Apple Valley",
	"stocks":0,
	"price":20,
	"onSale":false
},
{
	"name":"Papaya",
	"supplier":"Fruity Co.",
	"stocks":15,
	"price":60,
	"onSale":true
}

])

// 2. On Sale Fruits
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$count: "totalNumberofOnSaleFruits"}
])

// 3.Fruit Stock more than 20
db.fruits.aggregate([
	{$match: {stocks: {$gt: 20}}},
	{$count: "totalFruitStockMoreThan20"}	
])

// 4. Average Price per Supplier
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: { _id: "$supplier", "avgPricePerSupplier": {$avg: "$price"} }}
])

// 5. Highest price of fruit
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: { _id: "$supplier", "highestFruitPrice": {$max: "$price"} }}
])

// 6. Lowest price of fruit
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: { _id: "$supplier", "lowestFruitPrice": {$min: "$price"} }}
])

